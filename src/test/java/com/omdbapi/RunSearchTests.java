package com.omdbapi;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/com/omdbapi/features",
        glue = "com.omdbapi.steps",
        plugin = {"pretty"}
)

public class RunSearchTests {

}
