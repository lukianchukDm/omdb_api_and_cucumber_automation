package com.omdbapi.apiClient;

import com.omdbapi.models.Movie;
import com.omdbapi.steps.Hooks;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.List;

public class Search extends Hooks {

    public static ThreadLocal<List<Movie>> movies = ThreadLocal.withInitial(() -> null);
    public static ThreadLocal<String> imdbID = ThreadLocal.withInitial(() -> null);

    public static ThreadLocal<List<Movie>> getMovies() {
        return movies;
    }

    public static ThreadLocal<String> getImdbID() {
        return imdbID;
    }

    public static void search(String term) {
        List<Movie> movies = new ArrayList<>();

        for (int i = 1; i <= 100; i++) {
            Response response = RestAssured.given(aux.getSpec().get()).queryParam("s", term).queryParam("page", i).get();
            JsonPath responseJSON = response.jsonPath();

            boolean isResponseTrue = responseJSON.getString("Response").equals("True");

            if (isResponseTrue) {
                for (int j = 0; j < responseJSON.getList("Search.Title").size(); j++) {
                    Movie movie = new Movie();

                    movie.setTitle(responseJSON.getList("Search.Title").get(j).toString());
                    movie.setYear(responseJSON.getList("Search.Year").get(j).toString());
                    movie.setImdbID(responseJSON.getList("Search.imdbID").get(j).toString());
                    movie.setType(responseJSON.getList("Search.Type").get(j).toString());
                    movie.setPoster(responseJSON.getList("Search.Poster").get(j).toString());

                    movies.add(movie);
                }
            } else if (movies.size() > 0) break;
            else throw new Error("Movie '" + term + "' not found!");
        }

        getMovies().set(movies);
    }

    public static String getIdByTitle(String title) {
        for (Movie movie : getMovies().get()) {
            if (movie.getTitle().equals(title)) return movie.getImdbID();
        }
        return null;
    }

    public static Response getDetailsById(String id) {
        return RestAssured.given(aux.getSpec().get()).queryParam("i", id).get();
    }

    public static Response getByTitle(String title) {
        return RestAssured.given(aux.getSpec().get()).queryParam("t", title).get();
    }

}
