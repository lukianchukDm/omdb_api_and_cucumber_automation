package com.omdbapi.steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

import static com.omdbapi.apiClient.Search.aux;
import static com.omdbapi.apiClient.Search.*;
import static com.omdbapi.asserts.SearchAsserts.*;

public class SearchSteps {

    @When("^User searches (.*)$")
    public void userSearchesTerm(String term) {
        search(term);
    }

    @Then("^The result should contain at least (\\d+) items$")
    public void theResultShouldContainAtLeastNumberOfItems(int expNumber) {
        int actNumber = getMovies().get().size();
        assertResultsContainAtLeastNumberOfItems(actNumber, expNumber);
    }

    @Then("^The results contain following titles (.*)$")
    public void theResultsContainFollowingItems(String items) {
        assertResultsContainItems(items);
    }

    @Then("^Get details of the movie with the title (.*)$")
    public void getDetailsOfTheMovieWithTheTitle(String title) {
        String id = getIdByTitle(title);

        Response response = getDetailsById(id);

        aux.getResponse().set(response);
    }

    @Then("^The movie should have valid release date (.*) and director (.*)$")
    public void theMovieShouldHaveValidReleaseDateAndDirector(String expReleaseDate, String expDirectorName) {
        softAssertMovieHasValidReleaseDateAndDirector(expReleaseDate, expDirectorName);
    }

    @When("^User gets item by title (.*)$")
    public void userGetsItemByTitleTitle(String title) {
        Response response = getByTitle(title);

        response.print();

        aux.getResponse().set(response);
    }

    @Then("^The plot contains following (.*) and has a runtime of (.*) minutes$")
    public void thePlotContainsFollowingPlotAndHasARuntimeOfRuntimeMinutes(String expPlot, String expRuntime) {
        softAssertMovieHasValidPlotAndRuntime(expPlot, expRuntime);
    }
}
