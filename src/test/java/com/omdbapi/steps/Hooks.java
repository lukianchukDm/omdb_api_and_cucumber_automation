package com.omdbapi.steps;

import com.omdbapi.Aux;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.testng.asserts.SoftAssert;

public class Hooks {

    public static Aux aux;
    public static SoftAssert softAssert;

    @Before
    public void setUp() {
        if (aux == null)
            aux = new Aux();

        RequestSpecification spec = new RequestSpecBuilder()
                .setBaseUri("http://www.omdbapi.com/")
                .build();
        spec.queryParam("apikey", "c7517840");

        aux.getSpec().set(spec);

        aux.getSoftAssert().set(new SoftAssert());
        softAssert = aux.getSoftAssert().get();
    }

    @After
    public void tearDown() {
        aux.getSoftAssert().get().assertAll();
    }
}
