@search
Feature: OMDb search via API

  Scenario Outline: Search the term
    When User searches <term>
    Then The result should contain at least <number of items> items
    Then The results contain following titles <items>
    Then Get details of the movie with the title <title1>
    Then The movie should have valid release date <release date> and director <director>
    Then The movie should have valid release date <release date> and director <director>
    When User gets item by title <title2>
    Then The plot contains following <plot> and has a runtime of <runtime> minutes


    Examples:
      | term       | number of items | items                                                               | title1                                       | release date | director       | title2            | plot                                      | runtime |
# This scenario is gonna pass successfully
      | stem       | 30              | "The STEM Journals", "Activision: STEM - in the Videogame Industry" | Activision: STEM - in the Videogame Industry | 23 Nov 2010  | Mike Feurstein | The STEM Journals | science, technology, engineering and math | 22 min  |
# This scenario is gonna fail because there's no movie with this title
      | ..asfadfgg | 30              | "The STEM Journals", "Activision: STEM - in the Videogame Industry" |                                              |              |                |                   |                                           |         |
# This scenario is gonna fail on a soft assert, because there's no such title in the list
      | stem       | 30              | "..asfadfgg"                                                        |                                              |              |                |                   |                                           |         |
