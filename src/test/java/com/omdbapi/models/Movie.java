package com.omdbapi.models;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Movie {
    public String title;
    public String year;
    public String imdbID;
    public String type;
    public String poster;
}
