package com.omdbapi;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.asserts.SoftAssert;

public class Aux {
    public static ThreadLocal<RequestSpecification> spec = ThreadLocal.withInitial(() -> null);
    public static ThreadLocal<Response> response = ThreadLocal.withInitial(() -> null);
    public static ThreadLocal<SoftAssert> softAssert = ThreadLocal.withInitial(() -> null);

    public ThreadLocal<RequestSpecification> getSpec() {
        return spec;
    }

    public ThreadLocal<Response> getResponse() {
        return response;
    }

    public ThreadLocal<SoftAssert> getSoftAssert() {
        return softAssert;
    }
}
