package com.omdbapi.asserts;

import com.omdbapi.models.Movie;
import com.omdbapi.steps.Hooks;
import io.restassured.path.json.JsonPath;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.omdbapi.apiClient.Search.getMovies;
import static org.testng.Assert.assertTrue;

public class SearchAsserts extends Hooks {

    public static void assertResultsContainAtLeastNumberOfItems(int actNumber, int expNumber) {
        assertTrue(actNumber >= expNumber,
                "The results do not contain at least" + expNumber + " items.\nActual number of items is " + actNumber + "\n");
    }

    public static void assertResultsContainItems(String expItems) {
        List<String> titles = getMovies().get()
                .stream().map(Movie::getTitle)
                .collect(Collectors.toList());

        for (String expItem : getListOfItemsFromString(expItems)) {
            assertTrue(titles.contains(expItem), "Title '" + expItem + "' is not in a list\n");
        }
    }

    public static void softAssertMovieHasValidReleaseDateAndDirector(String expReleaseDate, String expDirector) {
        JsonPath responseJSON = aux.getResponse().get().jsonPath();

        softAssert.assertEquals(responseJSON.getString("Released"), expReleaseDate);
        softAssert.assertEquals(responseJSON.getString("Director"), expDirector);
    }

    public static void softAssertMovieHasValidPlotAndRuntime(String expPlot, String expRuntime) {
        JsonPath responseJSON = aux.getResponse().get().jsonPath();

        softAssert.assertTrue(responseJSON.getString("Plot").contains(expPlot));
        softAssert.assertEquals(responseJSON.getString("Runtime"), expRuntime);
    }

    private static List<String> getListOfItemsFromString(String items) {
        List<String> list = new ArrayList<>();

        Pattern p = Pattern.compile("\"([^\"]*)\"");
        Matcher m = p.matcher(items);
        while (m.find()) {
            list.add(m.group(1));
        }

        return list;
    }
}
